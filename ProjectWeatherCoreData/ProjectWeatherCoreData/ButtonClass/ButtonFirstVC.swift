//
//  ButtonFirstVC.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 30.08.2020.
//  Copyright © 2020 Михаил Копейкин. All rights reserved.
//

import Foundation
import UIKit

class ButtonFirstVC: UIButton {
    
    required init?(coder aDecoder: NSCoder) {
           super.init(coder: aDecoder)
           initializeBUtton()
    }
    override init(frame: CGRect) {
        super.init(frame:frame)
        initializeBUtton()
    }
    func initializeBUtton(){
        self.titleLabel?.textAlignment = .center
        self.setTitleColor(.white, for: .normal)
        self.sendActions(for: .touchUpInside)
    }
    
}

