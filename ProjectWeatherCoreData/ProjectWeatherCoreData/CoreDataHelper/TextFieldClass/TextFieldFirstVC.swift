//
//  TextFieldFirstVC.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 26.08.2020.
//  Copyright © 2020 Михаил Копейкин. All rights reserved.
//

import Foundation
import UIKit

class TextFieldFVC: UITextField {
    
    required init?(coder aDecoder: NSCoder) {
           super.init(coder: aDecoder)
           initializeTextFiekd()
       }
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        initializeTextFiekd()
    }
    
    func initializeTextFiekd() {
        self.textColor = .black
        self.keyboardType = .numbersAndPunctuation
        self.textAlignment = .center
        self.backgroundColor = .white
        self.font = .systemFont(ofSize: 20)
        self.clearButtonMode = .always
        self.contentVerticalAlignment = .center
    }
}
