//
//  CoreDataHelper.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 22.12.2020.
//  Copyright © 2020 Михаил Копейкин. All rights reserved.
//

import UIKit
import CoreData

class CoreDataHelper {
    
     init(){}
    
    //для доступа к контексту управляемого объекта нашего приложения
    static let context: NSManagedObjectContext = {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            fatalError()
        }

        let persistentContainer = appDelegate.persistentContainer
        let context = persistentContainer.viewContext

        return context
    }()
    
    //тут CoreDate сохраняем в базе
    static func saveLatLon(dataLat:String,dataLon:String){
        
        //создаем dataLaLo
        let dataLaLo = DataLatLon(entity: DataLatLon.entity(), insertInto: context)
        
        dataLaLo.setValue(dataLat, forKey: "dataLat")
        dataLaLo.setValue(dataLon, forKey: "dataLon")
        
        let lat = dataLaLo.value(forKey: "dataLat")
        let lon = dataLaLo.value(forKey: "dataLon")
        
        print("\(String(describing: lat)) и \(String(describing: lon)) ")
        //сохраняем имя в CoreDate
        
        if context.hasChanges{
            do{
                try context.save()
                
                //infoLatLon.append(dataLaLo)
            } catch let error as NSError {
                print("Could not save \(error), \(error.userInfo)")
            }
        }
        
    }
    //удаление данных из кор даты
    static func delete() {
        do{
            let results = try context.fetch(DataLatLon.fetchRequest())
            print(results)
            
            for result in results as! [DataLatLon] {
                
                context.delete(result)
                
            }
            try context.save()
            
        } catch let error as NSError {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    //получаем данные
    static func retrieveNotes() -> [DataLatLon] {
    do{
        let results = try context.fetch(DataLatLon.fetchRequest())
        print(results)
        
        return results as! [DataLatLon]

    } catch let error as NSError {
        print("Could not save \(error), \(error.userInfo)")
        return []
    }
    }
}
