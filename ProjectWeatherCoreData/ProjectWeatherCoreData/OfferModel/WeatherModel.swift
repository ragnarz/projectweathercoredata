//
//  WeatherModel.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 08.09.2020.
//  Copyright © 2020 Михаил Копейкин. All rights reserved.
//

import Foundation

class WeatherModel:Codable{
    
    var icon:String?
    var description:String?
}
