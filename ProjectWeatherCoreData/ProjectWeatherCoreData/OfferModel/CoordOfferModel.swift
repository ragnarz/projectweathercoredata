//
//  CoordOfferModel.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 02.09.2020.
//  Copyright © 2020 Михаил Копейкин. All rights reserved.
//

import Foundation

class CoordOfferModel: Codable {
    var  lat:Double?
    var  lon:Double?
}
