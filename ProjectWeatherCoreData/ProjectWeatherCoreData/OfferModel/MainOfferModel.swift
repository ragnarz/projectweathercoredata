//
//  MainOfferModel.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 02.09.2020.
//  Copyright © 2020 Михаил Копейкин. All rights reserved.
//

import Foundation

class MainOfferModel: Codable {
    var temp:Double?
    var temp_min:Double?
    var temp_max:Double?
    var feels_like:Double?
    var humidity:Double?
    var grnd_level:Double?
}
