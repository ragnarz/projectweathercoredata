//
//  ListOfferModel.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 02.09.2020.
//  Copyright © 2020 Михаил Копейкин. All rights reserved.
//

import Foundation

class ListOfferModel: Codable {
    var dt:Int?
    var main:MainOfferModel?
    var weather:[WeatherModel]?
    var dt_txt:String?
    var pop: Double?
    var sys: SysModel?
    var wind:WindModel?
    
    
}
