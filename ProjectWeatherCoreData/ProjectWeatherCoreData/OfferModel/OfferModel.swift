//
//  OfferModel.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 02.09.2020.
//  Copyright © 2020 Михаил Копейкин. All rights reserved.
//

import Foundation

class OfferModel:Codable {
    
    var cod:String?
    var message: Int?
    var cnt: Int?
    var list:[ListOfferModel]?
    var city:CityModel?
    
}


