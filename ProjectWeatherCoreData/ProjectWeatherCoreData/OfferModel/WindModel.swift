//
//  WindModel.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 22.11.2020.
//  Copyright © 2020 Михаил Копейкин. All rights reserved.
//

import Foundation

struct WindModel:Codable {
    let speed: Double?
    let deg: Double?
}
