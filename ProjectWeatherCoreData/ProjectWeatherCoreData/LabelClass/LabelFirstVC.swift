//
//  LabelFirstVC.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 26.08.2020.
//  Copyright © 2020 Михаил Копейкин. All rights reserved.
//

import Foundation

import UIKit

class LableFirstVC: UILabel {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeLabel()
    }
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        initializeLabel()
    }
    
    
    func initializeLabel() {
        self.textAlignment = .center
        self.font = .systemFont(ofSize: 24)
        self.textColor = .white
        self.numberOfLines = 0
        
    }
    
}
