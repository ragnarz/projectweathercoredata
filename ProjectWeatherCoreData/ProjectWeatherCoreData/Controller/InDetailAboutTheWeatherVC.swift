//
//  InDetailAboutTheWeatherVC.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 12.11.2020.
//  Copyright © 2020 Михаил Копейкин. All rights reserved.
//

import UIKit
import SnapKit

class InDetailAboutTheWeatherVC: UIViewController {
    @IBOutlet weak var dateLable: UILabel!
    @IBOutlet weak var nameCityLable: UILabel!
    @IBOutlet weak var iconWeatherImag: UIImageView!
    @IBOutlet weak var tempTodayLable: UILabel!
    @IBOutlet weak var tempMinLAble: UILabel!
    @IBOutlet weak var tempMaxLAble: UILabel!
    @IBOutlet weak var weatherDescriptionLable: UILabel!
    @IBOutlet weak var feltLable: UILabel!
    @IBOutlet weak var feelsLike: UILabel!
    @IBOutlet weak var humidityLable: UILabel!
    @IBOutlet weak var humidityIntLable: UILabel!
    @IBOutlet weak var probabilityOfPrecipitationLable: UILabel!
    @IBOutlet weak var popLable: UILabel!
    @IBOutlet weak var pressureLable: UILabel!
    @IBOutlet weak var grndLevelLable: UILabel!
    @IBOutlet weak var windSpeedLable: UILabel!
    @IBOutlet weak var speedLable: UILabel!
    @IBOutlet weak var thirdViiewCentrVC: UIView!
    @IBOutlet weak var thirdViiewCentrVCLayer2: UIView!
    @IBOutlet weak var MinMaxLable: UILabel!
    @IBOutlet weak var iconSelfViewimagView: UIImageView!
    
    var dateVCLable = ""
    var  nameCityVCLable = ""
    var  iconWeatherVCImag = ""
    var  tempTodayLVCable = ""
    var  tempMinVCLAble = ""
    var  tempMaxVCLAble = ""
    var  weatherVCDescriptionLable = ""
    var  feelsVCLike = ""
    var  humidityVCIntLable = ""
    var  popVCLable = ""
    var  grndVCLevelLable = ""
    var  speedVCLable = ""
    var  podChangeDayNight = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        customizationViewNavContr()
        addAtributsForLable()
        
        
       
    }
    
    func customizationViewNavContr() {
        
        if  podChangeDayNight == "d" {
            let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
            backgroundImage.image = UIImage(named: "Rectangle 9 (1)")
            iconSelfViewimagView.image = UIImage(named: "Group 13")
            self.view.insertSubview(backgroundImage, at: 0)
        }else {
            let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
            backgroundImage.image = UIImage(named: "Rectangle 9 (2)")
            iconSelfViewimagView.image = UIImage(named: "Group 22")
            changeBackgaundLable()
            self.view.insertSubview(backgroundImage, at: 0)
        }
        self.navigationItem.title = "Приложение погода"
    }
    
    func changeBackgaundLable(){
        self.dateLable.textColor = .white
        self.nameCityLable.textColor = .white
        self.tempTodayLable.textColor = .white
        self.tempMinLAble.textColor = .white
        self.tempMaxLAble.textColor = .white
        self.weatherDescriptionLable.textColor = .white
        self.feelsLike.textColor = .white
         self.feltLable.textColor = .white
        self.humidityIntLable.textColor = .white
         self.humidityLable.textColor = .white
        self.popLable.textColor = .white
         self.probabilityOfPrecipitationLable.textColor = .white
         self.pressureLable.textColor = .white
        self.grndLevelLable.textColor = .white
        self.speedLable.textColor = .white
        self.windSpeedLable.textColor = .white
        self.MinMaxLable .textColor = .white
        
    }
    
    func addAtributsForLable(){
        self.dateLable.text = getTimForDate(dateVCLable)
               self.nameCityLable.text = nameCityVCLable
               self.tempTodayLable.text = "\(tempTodayLVCable)°C"
               self.tempMinLAble.text = "\(tempMinVCLAble) /"
               self.tempMaxLAble.text = "  \(tempMaxVCLAble)"
               self.weatherDescriptionLable.text = weatherVCDescriptionLable
               self.feelsLike.text = "\(feelsVCLike)°C"
               self.humidityIntLable.text = "\(humidityVCIntLable)%"
               self.popLable.text = "\(popVCLable)%"
               self.grndLevelLable.text = grndVCLevelLable
               self.speedLable.text = "\(speedVCLable)М/с"
               self.MinMaxLable .text = "Мин°C  /  Макс°C "
        
               let iconURL = URL(string: "http://openweathermap.org/img/wn/\(self.iconWeatherVCImag)@2x.png")
                               let dataIcon = try? Data(contentsOf: iconURL!)
               self.iconWeatherImag.image = UIImage(data: dataIcon!)
       
    }
    
    func setupAttributs (atr:InDetailAboutTheWeatherViewAttribut){
           dateVCLable = (atr.date)
           nameCityVCLable = (atr.nameCity)
           iconWeatherVCImag = (atr.iconWeather)
           tempTodayLVCable = (atr.tempToday)
           tempMinVCLAble = (atr.tempMin)
           tempMaxVCLAble = (atr.tempMax)
           weatherVCDescriptionLable  = (atr.weatherDescription)
           feelsVCLike = (atr.felsLike)
           humidityVCIntLable = (atr.humidityInt)
           popVCLable  = (atr.pop)
           grndVCLevelLable = (atr.grndLevel)
           speedVCLable = (atr.speed)
        podChangeDayNight = (atr.pod)

       }
    private  func getTimForDate(_ date:String?) -> String {
           guard let inputDate = date else {return ""}
           
           let dateFormatterGet = DateFormatter()
           dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"

           let dateFormatterPrint = DateFormatter()
           dateFormatterPrint.dateFormat = "HH:mm dd.MM.yyyy"

           let date = dateFormatterGet.date(from: inputDate)
           let dateStr = dateFormatterPrint.string(from: date!)

           return dateStr
       }
    
}

struct InDetailAboutTheWeatherViewAttribut {
    let date:String
    let nameCity:String
    let iconWeather:String
    let tempToday:String
    let tempMin:String
    let tempMax:String
    let weatherDescription:String
    let felsLike:String
    let humidityInt:String
    let pop:String
    let grndLevel:String
    let speed:String
    let pod:String

}
