//
//  ListOfForecastDatesVC.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 09.09.2020.
//  Copyright © 2020 Михаил Копейкин. All rights reserved.
//  Всё что закоментированно этот код идёт под тейбл Вью

import UIKit
import SnapKit

class ListOfForecastDatesVC: UIViewController {
    
    var lat = ""
    var lon = ""
    var units = "metric"
    var podChangeBackGRDayNight = ""
    
    var iconSelfViewimag:UIImageView = {
       let iconImagView = UIImageView()
        iconImagView.frame = CGRect(x: 210.07, y: 72.28, width: 157.72, height: 157.72)
        return iconImagView
    }()
    
    fileprivate var contentView:MainVIewColVC!
//    fileprivate var contentView:MainVIew!
//
    var dataIsReady:Bool = false
//    //для того что бы сразу назначить данные которые приходять из модели более подробно ниже
        var offerModelAttributs:[ListOfForecastDatesCollectionViewCellAttribut]! {
            didSet{
                DispatchQueue.main.async {
                    self.setupBackGraundView()
                    self.contentView.firstViewCenterClass.coltctionView.reloadData()
                    self.contentView.firstViewCenterClass.setup(attr: self.firstViewCenterClassAttribut[0])
                }
    
            }
        }
    
//    var offerModelAttributs:[ListOfForecastDatesTableViewCellAttribut]! {
//        didSet{
//            DispatchQueue.main.async {
//                self.contentView.tableView.reloadData()
//
//            }
//
//        }
//    }
    
//    var нeaderOfferModelAttributs:[FirstCustomTableViewCellAttribut]!
    var inDetailAboutTheWeathVCAttributs:[InDetailAboutTheWeatherViewAttribut]!
    var firstViewCenterClassAttribut:[FirstViewCenterClassAttribut]!
    
    var offerModel:OfferModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.createUI()
        self.contentView.firstViewCenterClass.coltctionView.delegate = self
        self.contentView.firstViewCenterClass.coltctionView.dataSource = self
        self.updateResults()
        self.setupNavigationBar()
         
        
        
    }
    //замена на кастомную view
    
        func createUI() {
            let main = MainVIewColVC()
            self.view = main
            self.contentView = main
        }
    
//    func createUI() {
//        let main = MainVIew()
//        self.view = main
//        self.contentView = main
//    }
    //настройка навигации
    fileprivate func  setupNavigationBar(){
        
        self.navigationItem.title = "Приложение погода"
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Назад", style:.plain, target: self, action: nil)
    }
  
    //настройка фона view основной
    fileprivate func setupBackGraundView() {
        
        if  podChangeBackGRDayNight == "d" {
            let backgroundImage = UIImageView(frame: self.contentView.bounds)
            backgroundImage.image = UIImage(named: "Rectangle 9 (1)")
            backgroundImage.addSubview(iconSelfViewimag)
            iconSelfViewimag.image = UIImage(named: "Group 13")
            self.iconSelfViewimag.snp.makeConstraints { (make) in
                make.size.equalTo(CGSize(width: 157.72, height: 157.72))
                make.rightMargin.equalTo(backgroundImage).offset(-7.21)
                make.topMargin.equalTo(backgroundImage).offset(10.28)
            }
            
            self.contentView.firstViewCenterClass.cityLable.textColor = #colorLiteral(red: 0.2823250294, green: 0.2823709249, blue: 0.2901571691, alpha: 1)
            self.contentView.firstViewCenterClass.tempLable.textColor = #colorLiteral(red: 0.2823250294, green: 0.2823709249, blue: 0.2901571691, alpha: 1)
            self.contentView.firstViewCenterClass.timeLable.textColor = #colorLiteral(red: 0.2823250294, green: 0.2823709249, blue: 0.2901571691, alpha: 1)
            self.contentView.insertSubview(backgroundImage, at: 0)
            
        }else {
            let backgroundImage = UIImageView(frame: self.contentView.bounds)
            backgroundImage.image = UIImage(named: "Rectangle 9 (2)")
            backgroundImage.addSubview(iconSelfViewimag)
            iconSelfViewimag.image = UIImage(named: "Group 22")
            self.contentView.insertSubview(backgroundImage, at: 0)
            
            self.iconSelfViewimag.snp.makeConstraints { (make) in
                make.size.equalTo(CGSize(width: 157.72, height: 157.72))
                make.rightMargin.equalTo(backgroundImage).offset(-7.21)
                make.topMargin.equalTo(backgroundImage).offset(10.28)
            }
            
        }
        
    }
    //обновление результатов
    func updateResults(){

        let units = "metric"
        //получаем данные из кор даты
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

        do {
            let results = try context.fetch(DataLatLon.fetchRequest())
            print(results)
            //что бы получить то что надо из кор даты надо пробежатся по циклу и по ключу вытянуть то что надо
            for result in results as! [DataLatLon] {
                lat = (result.value(forKey: "dataLat") as? String)!
                lon = (result.value(forKey: "dataLon") as? String)!
            }

            NetworkManager.sharet.getWeather(latitude: lat, longitude: lon, units: units, result: { (model) in

                if model != nil {
                    self.dataIsReady = true
                    self.offerModel = model


                    self.createOfferAttributes(model: model!)
                }else{
                    self.alert(title: "Внимание", massag: "Чтото пошло не так,определите автомотически либо попробуйте ввестив в таком формате 00.0000000 ", style: .alert)
                }
            })

        } catch let error as NSError {
            print("Could not save \(error), \(error.userInfo)")
        }

    }
    
    //алерт который вызывается если текст филд пустой
    func alert (title:String, massag:String, style:UIAlertController.Style) {
        let alertControler = UIAlertController(title: title, message: massag, preferredStyle: style)
        let action = UIAlertAction(title: "ок", style: .default, handler: nil)
        
        alertControler.addAction(action)
        self.present(alertControler, animated: true, completion: nil)
    }
    
    //добавление в лист офер модель данных из модели 
    private func createOfferAttributes(model:OfferModel){
        var _offerModelAttributs = [ListOfForecastDatesCollectionViewCellAttribut]()
//        var _нeaderOfferModelAttributs = [FirstCustomTableViewCellAttribut]()
        var _inDetailAboutTheWeathVCAttributs = [InDetailAboutTheWeatherViewAttribut]()
        var _firstViewCenterClassAttribut = [FirstViewCenterClassAttribut]()
        let city = model.city!.name!
        self.podChangeBackGRDayNight = model.list![0].sys!.pod!.description


        for element in model.list! {
            _offerModelAttributs.append(ListOfForecastDatesCollectionViewCellAttribut(temp: element.main!.temp!.description,
                                                                                 cityName: city,
                                                                                 icon: element.weather![0].icon!.description,

                                                                                 time: element.dt_txt!))

            _inDetailAboutTheWeathVCAttributs.append(InDetailAboutTheWeatherViewAttribut(date: element.dt_txt!,
                                                                                               nameCity: city,
                                                                                               iconWeather: element.weather![0].icon!.description,
                                                                                               tempToday: element.main!.temp!.description,
                                                                                               tempMin: element.main!.temp_min!.description,
                                                                                               tempMax: element.main!.temp_max!.description,
                                                                                               weatherDescription: element.weather![0].description!.description,
                                                                                               felsLike: element.main!.feels_like!.description,
                                                                                               humidityInt: element.main!.humidity!.description,
                                                                                               pop: element.pop!.description,
                                                                                               grndLevel: element.main!.grnd_level!.description,
                                                                                               speed: element.wind!.speed!.description,
                                                                                               pod: element.sys!.pod!.description))


        }
        self.offerModelAttributs = _offerModelAttributs
        self.inDetailAboutTheWeathVCAttributs = _inDetailAboutTheWeathVCAttributs
        
        _firstViewCenterClassAttribut.append(FirstViewCenterClassAttribut(firstTemp: model.list![0].main!.temp!.description,
                                                                          firstCityName: city,
                                                                          firstIcon: model.list![0].weather![0].icon!.description,
                                                                          firstTime: model.list![0].dt_txt!))
        
        self.firstViewCenterClassAttribut = _firstViewCenterClassAttribut

//        _нeaderOfferModelAttributs.append(FirstCustomTableViewCellAttribut(firstTemp: model.list![0].main!.temp!.description,
//                                                                            firstCityName: city,
//                                                                            firstIcon: model.list![0].weather![0].icon!.description))
//
//        self.нeaderOfferModelAttributs = _нeaderOfferModelAttributs



    }
    
    
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 200
//    }
//
//    //MARK:-UITableViewDataSource
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//
//
//
//        if dataIsReady  {
//            return offerModel!.list!.count
//        }
//        else {
//            return 0
//        }
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        let cell = tableView.dequeueReusableCell(withIdentifier: "ListOfForecastDatesTableViewCell1") as! ListOfForecastDatesTableViewCell
//
//        cell.backgroundColor = #colorLiteral(red: 0.2390094101, green: 0.5961227417, blue: 0.9057689309, alpha: 1)
//
//        //добавление данных через атрибуты вся логика в файле ячейка
//        cell.setup(attr: self.offerModelAttributs[indexPath.row])
//
//        return cell
//    }
//
//
//
//    //сделали такой заголовок что бы отаброзить данные за один день
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        if dataIsReady == true {
//
//            let viewFirstCustomTableViewCell = Bundle.main.loadNibNamed("FirstCustomTableViewCell", owner: self, options: nil)?[0] as! FirstCustomTableViewCell
//
//            //добавление данных через атрибуты вся логика в файле ячейки
//            viewFirstCustomTableViewCell.setup(attr: self.нeaderOfferModelAttributs[section])
//
//            return viewFirstCustomTableViewCell
//        }
//        return nil
//    }
//
//
//    //задали размер заголовка
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        if dataIsReady == true{
//            return 300
//        }else {
//            return 0
//        }
//    }
//
//
//    //по нажатию на я чейку происходит переход на 3-й контролер и по индекс пасу отображается
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//        let cell = tableView.cellForRow(at: indexPath)
//
//        if cell != nil {
//
//            let inDetailAboutTheWeathVC:UIViewController = {
//
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let vc = storyboard.instantiateViewController(withIdentifier: "InDetailAboutTheWeatherVC")
//                guard let vCInDetailAboutTheWeath = vc as? InDetailAboutTheWeatherVC else {return InDetailAboutTheWeatherVC()}
//
//                //добавление данных через атрибуты вся логика в файле ячейки
//                vCInDetailAboutTheWeath.setupAttributs(atr: inDetailAboutTheWeathVCAttributs[indexPath.row])
//
//
//                return vCInDetailAboutTheWeath
//            }()
//
//            self.navigationController?.pushViewController(inDetailAboutTheWeathVC, animated: true)
//        }
//
//    }
    
    
}

extension ListOfForecastDatesVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if dataIsReady  {
                    return offerModel!.list!.count
                }
                else {
                    return 0
                }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ListOfForecastDatesCollectionViewCell", for: indexPath) as! ListOfForecastDatesCollectionViewCell

        //добавление данных через атрибуты вся логика в файле ячейка
        cell.secondViewCell.setup(attr: self.offerModelAttributs[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 61.85, height: 113.52)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath)
        
                if cell != nil {
        
                    let inDetailAboutTheWeathVC:UIViewController = {
        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "InDetailAboutTheWeatherVC")
                        guard let vCInDetailAboutTheWeath = vc as? InDetailAboutTheWeatherVC else {return InDetailAboutTheWeatherVC()}
        
                        //добавление данных через атрибуты вся логика в файле ячейки
                        vCInDetailAboutTheWeath.setupAttributs(atr: inDetailAboutTheWeathVCAttributs[indexPath.row])
        
        
                        return vCInDetailAboutTheWeath
                    }()
        
                    self.navigationController?.pushViewController(inDetailAboutTheWeathVC, animated: true)
                }
        
    }
}
