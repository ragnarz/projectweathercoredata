//
//  DataInputVC.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 25.08.2020.
//  Copyright © 2020 Михаил Копейкин. All rights reserved.
//

import UIKit
import CoreLocation
import SnapKit

class DataInputVC: UIViewController,UITextFieldDelegate {
    
    var viewCenter = ViewCenterClass()
    var viewCentrLayer2 = ViewCenterLayer2Class()
    var lableFVC = LableFirstVC()
    var lableAvtoFVC = LableFirstVC()
    var latitudeTextFieldFVC = TextFieldFVC()
    var longitudeTextFieldFVC = TextFieldFVC()
    var buttonFirstVC = ButtonFirstVC()
    var switchFirstVC = SwitchFirstVC()
    
   
    
    let locationManager = CLLocationManager()
    
    var infoLatLon = [DataLatLon]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Назад", style:.plain, target: self, action: nil)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.white]
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "Rectangle 9")
        self.view.insertSubview(backgroundImage, at: 0)
        
        self.locationManager.delegate = self
        
        createViewCenter1()
        createGesterSwipDellkKyboard()
       
    }
    
    func createGesterSwipDellkKyboard() {
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.hideKeyboardOnSwipeDown))
                swipeDown.delegate = self
        swipeDown.direction =  UISwipeGestureRecognizer.Direction.down
                self.view.addGestureRecognizer(swipeDown)
    }
    
    @objc func hideKeyboardOnSwipeDown() {
            view.endEditing(true)
        }
    
    func createViewCenter1(){
        
        self.view.addSubview(viewCenter)
        
        viewCenter.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 280.34, height: 330.0))
            make.center.equalTo(self.view.center)
        }
        
        self.viewCenter.addSubview(viewCentrLayer2)
        
        viewCentrLayer2.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 262.25, height: 308.71))
            make.center.equalTo(self.viewCenter.snp.center)
        }
        createSwitch()
        createlableAvtoFVC()
        createlongitudeTextFieldFVC()
        createlatitudeTextField()
        createLable()
        createButtonFirstVC()
    }
    
    fileprivate func createSwitch() {
//        switchFirstVC.frame = CGRect(x: 187.63, y: 204.85, width: 0.0, height: 0.0)
        switchFirstVC.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        switchFirstVC.addTarget(self, action: #selector(isOn(target:)), for: .valueChanged)
        self.viewCentrLayer2.addSubview(switchFirstVC)
        
        switchFirstVC.snp.makeConstraints { (make) in
            make.edges.equalTo(UIEdgeInsets(top: 204.85, left: 187.63, bottom: 72.85, right: 23.63))
        }
    }
    
    @objc func isOn(target:UISwitch){
        if target.isOn{
            self.buttonFirstVC.isUserInteractionEnabled = false
            self.latitudeTextFieldFVC.isUserInteractionEnabled = false
            self.longitudeTextFieldFVC.isUserInteractionEnabled = false
            
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
            self.locationManager.requestWhenInUseAuthorization()
            self.locationManager.startUpdatingLocation()
            
        }else{
            
            self.buttonFirstVC.isUserInteractionEnabled = true
            self.latitudeTextFieldFVC.isUserInteractionEnabled = true
            self.longitudeTextFieldFVC.isUserInteractionEnabled = true
        }
    }
    
    fileprivate func createLable() {
        
        self.viewCentrLayer2.addSubview(lableFVC)

        lableFVC.textColor = UIColor(red: 0.282, green: 0.282, blue: 0.29, alpha: 1)
        lableFVC.font = UIFont(name: "SFProDisplay-Medium", size: 15)
        lableFVC.numberOfLines = 0
        lableFVC.lineBreakMode = .byWordWrapping
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 0.89
        // Line height: 16 pt
        lableFVC.attributedText = NSMutableAttributedString(string: "Введите широту и долготу\nместа где хотите узнать погоду", attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
        lableFVC.textAlignment = .center
        
        lableFVC.snp.makeConstraints { (make) in
                   make.size.equalTo(CGSize(width: 215, height: 32))
            make.edges.equalTo(UIEdgeInsets(top: 38.35, left: 23.63, bottom: 238.35, right: 23.63))
               }
    }
    
    fileprivate func createlableAvtoFVC() {
        
        self.viewCentrLayer2.addSubview(lableAvtoFVC)
        
        lableAvtoFVC.textColor = UIColor(red: 0.282, green: 0.282, blue: 0.29, alpha: 1)
        lableAvtoFVC.font = UIFont(name: "SFProDisplay-Light", size: 12)
        lableAvtoFVC.numberOfLines = 0
        lableAvtoFVC.lineBreakMode = .byWordWrapping
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1
        // Line height: 16 pt
        lableAvtoFVC.attributedText = NSMutableAttributedString(string: "Определять автоматически\nместопложение", attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
        
        lableAvtoFVC.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 165, height: 32))
            make.edges.equalTo(UIEdgeInsets(top: 204.85, left: 23.63, bottom: 71.85, right: 90.46))
           }
    }
    
    
    fileprivate func createlatitudeTextField(){
        
        self.viewCentrLayer2.addSubview(latitudeTextFieldFVC)
//        latitudeTextFieldFVC.keyboardType = .numberPad
        latitudeTextFieldFVC.placeholder = "Широта"
        latitudeTextFieldFVC.layer.cornerRadius = 13
        latitudeTextFieldFVC.clipsToBounds = true
        latitudeTextFieldFVC.delegate = self

        latitudeTextFieldFVC.snp.makeConstraints { (make) in
//         make.size.equalTo(CGSize(width: 215, height: 44))
            make.edges.equalTo(UIEdgeInsets(top: 90.85, left: 23.63, bottom: 173.85, right: 23.63))
        }
        
    }
    
    fileprivate func createlongitudeTextFieldFVC(){
        self.viewCentrLayer2.addSubview(longitudeTextFieldFVC)
//        longitudeTextFieldFVC.keyboardType = .numberPad
        longitudeTextFieldFVC.placeholder = "Долгота"
        longitudeTextFieldFVC.layer.cornerRadius = 13
        longitudeTextFieldFVC.clipsToBounds = true
        longitudeTextFieldFVC.delegate = self
    
        longitudeTextFieldFVC.snp.makeConstraints { (make) in
         make.size.equalTo(CGSize(width: 215, height: 44))
            make.edges.equalTo(UIEdgeInsets(top: 140.85, left: 23.63, bottom: 123.85, right: 23.63))
        }
    }
    
    fileprivate func createButtonFirstVC(){
        buttonFirstVC.frame = CGRect(x: 37.87, y: 261.48, width: 186.51, height: 37.87)
        buttonFirstVC.backgroundColor = UIColor(red: 0.129, green: 0.588, blue: 0.953, alpha: 1)
        buttonFirstVC.setTitle("Поиск", for:.normal)
        buttonFirstVC.layer.cornerRadius = 14
        buttonFirstVC.clipsToBounds = true
        
        buttonFirstVC.addTarget(self, action: #selector(transmissionOfLatitudeAndLongitudeInTheSecondVC), for: .touchUpInside)
        viewCentrLayer2.addSubview(buttonFirstVC)
    }
    //сохранение в кор дате введенных данных и переход на 2-й контроллер
    @objc func transmissionOfLatitudeAndLongitudeInTheSecondVC (_sender:Any) {
        
        if (latitudeTextFieldFVC.text != "") , longitudeTextFieldFVC.text != "" {
            
            let lat = latitudeTextFieldFVC.text!
            let lon = longitudeTextFieldFVC.text!
            
            //Сщхранение в базе CoreDate
            CoreDataHelper.saveLatLon(dataLat: lat, dataLon: lon)
            
            let listOfForecastDatesVC = ListOfForecastDatesVC()
            self.navigationController?.pushViewController(listOfForecastDatesVC, animated: true)
        } else {
            
            self.alert(title: "Внимание", massag: "Введите широту и долготу в таком формате 00.0000000", style: .alert)
        }
        
    }
    
    // тут будет форматироыватся строка которая  приходит из текс филда  на выходе отформатированный стринг
//   private func format(with mask: String, numberTextField: String) -> String {
//        let numbers = numberTextField.replacingOccurrences(of: "[^-0-9]", with: "", options: .regularExpression)
//        var result = ""
//        var index = numbers.startIndex // numbers iterator
//
//        // iterate over the mask characters until the iterator of numbers ends
//        for ch in mask where index < numbers.endIndex {
//            if ch == "X" {
//                // mask requires a number in this place, so take the next one
//                result.append(numbers[index])
//
//                // move numbers iterator to the next index
//                index = numbers.index(after: index)
//
//            } else {
//                result.append(ch) // just append a mask character
//            }
//        }
//        return result
//    }
   
    
    
    //сдесь будет ограничение ввода текст филда
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        
//            let newString = (text as NSString).replacingCharacters(in: range, with: string)
//            textField.text = format(with: "XX.XXXXXXX", numberTextField: newString)

        
        
        
//        let currentCharacterCount  = textField.text?.count ?? 0
//        if (range.length + range.location > currentCharacterCount){
//
//            return false
//        }
//        let newLenght = currentCharacterCount + string.count - range.length
//
//        return newLenght <= 10
      
        
        
        
        
//        // max 2 fractional digits allowed
//        let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
//                let regex = try! NSRegularExpression(pattern: "\\..{10,}", options: [])
//        let matches = regex.matches(in: newText, options:[], range:NSMakeRange(0, newText.count))
//                guard matches.count == 0 else { return false }
//
//                switch string {
//                case "-","0","1","2","3","4","5","6","7","8","9":
//                    return true
//                case ".":
//                    let array = textField.text?.map { String($0) }
//                    var decimalCount = 0
//                    for character in array! {
//                        if character == "." {
//                            decimalCount += 1
//                        }
//                    }
//                    if decimalCount == 1 {
//                        return false
//                    } else {
//                        return true
//                    }
//                default:
//                    let array = string.map { String($0) }
//                    if array.count == 0 {
//                        return true
//                    }
//                    return false
//                }
        if textField.text == self.latitudeTextFieldFVC.text || (self.longitudeTextFieldFVC.text != nil) {
            
            switch textField.text {
            case self.latitudeTextFieldFVC.text:
                let text = (textField.text ?? "") as NSString
                let newText = text.replacingCharacters(in: range, with: string)
                
                if let regex = try? NSRegularExpression(pattern: "^[-]?$*[0-9]{0,1}?$*[0-9]{0,1}?$*((\\.)[0-9]{0,7})?$", options: .caseInsensitive)  {
                    return regex.numberOfMatches(in: newText, options: .reportProgress, range: NSRange(location: 0, length: (newText as NSString).length)) > 0
                }
            case self.longitudeTextFieldFVC.text:
                let text = (textField.text ?? "") as NSString
                let newText = text.replacingCharacters(in: range, with: string)
                
                if let regex = try? NSRegularExpression(pattern: "^[-]?$*[0-1]{0,1}?$*[0-9]{0,1}?$*[0-9]{0,1}?$*((\\.)[0-9]{0,7})?$", options: .caseInsensitive)  {
                    return regex.numberOfMatches(in: newText, options: .reportProgress, range: NSRange(location: 0, length: (newText as NSString).length)) > 0
                }
            default:
                break
            }
        }
        return false
    }
    

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
           return true
       }
    
    //алерт который вызывается если текст филд пустой
    func alert (title:String, massag:String, style:UIAlertController.Style) {
        let alertControler = UIAlertController(title: title, message: massag, preferredStyle: style)
        let action = UIAlertAction(title: "ок", style: .default, handler: nil)
        
        alertControler.addAction(action)
        self.present(alertControler, animated: true, completion: nil)
    }
    
    //удаление как текста так и данных из кор даты это метод вызывается когда нажимается кнопка отчистки в текстфилде
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        CoreDataHelper.delete()
        
        return true
    }
    
    //что бы масив остовался с данными которые ввели
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.barStyle = .black
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
        statusBarView.backgroundColor = statusBarColor
        view.addSubview(statusBarView)
  
        let dateInCoreDate = CoreDataHelper.retrieveNotes()
        
        for result in dateInCoreDate {
            latitudeTextFieldFVC.text = result.value(forKey: "dataLat") as? String
            longitudeTextFieldFVC.text = result.value(forKey: "dataLon") as? String
        }
    }
}
extension DataInputVC:CLLocationManagerDelegate,UIGestureRecognizerDelegate {
    
    //MARK:-CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print(manager.location!)
        let currentLocation = locations.last!
        
        if currentLocation.horizontalAccuracy > 0 {
            //stop updation to save batery liv
            locationManager.stopUpdatingLocation()
            
            let cords = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude, currentLocation.coordinate.longitude)
            
            print(cords)

            if (latitudeTextFieldFVC.text == "") , longitudeTextFieldFVC.text == ""  {
  
                var lat = String(cords.latitude)
                var lon = String(cords.longitude)
     
                let range = lat.index(lat.endIndex, offsetBy: -8)..<lat.endIndex
                lat.removeSubrange(range)
                
                let range2 = lon.index(lon.endIndex, offsetBy: -8)..<lon.endIndex
                lon.removeSubrange(range2)

                self.latitudeTextFieldFVC.text = lat
                self.longitudeTextFieldFVC.text = lon
                
                //сохранение в кордате
                CoreDataHelper.saveLatLon(dataLat: lat , dataLon: lon)
                
                self.switchFirstVC.isOn = false
                self.buttonFirstVC.isUserInteractionEnabled = true
                self.latitudeTextFieldFVC.isUserInteractionEnabled = true
                self.longitudeTextFieldFVC.isUserInteractionEnabled = true
                 
            }else if  (latitudeTextFieldFVC.text != "") , longitudeTextFieldFVC.text != "" {
                self.switchFirstVC.isOn = false
                //удаление из CoreDate
                CoreDataHelper.delete()
                
                var lat = String(cords.latitude)
                var lon = String(cords.longitude)
            
                let range = lat.index(lat.endIndex, offsetBy: -8)..<lat.endIndex
                lat.removeSubrange(range)
                
                let range2 = lon.index(lon.endIndex, offsetBy: -8)..<lon.endIndex
                lon.removeSubrange(range2)
                
                self.latitudeTextFieldFVC.text = lat
                self.longitudeTextFieldFVC.text = lon
                
                //добавляем в масив и сохранение в кордате
                CoreDataHelper.saveLatLon(dataLat: lat , dataLon: lon)
                
                self.switchFirstVC.isOn = false
                self.buttonFirstVC.isUserInteractionEnabled = true
                self.latitudeTextFieldFVC.isUserInteractionEnabled = true
                self.longitudeTextFieldFVC.isUserInteractionEnabled = true

            }else{
                self.alert(title: "Внимание", massag: "Коардинаты не определились, попробуйте ввести вручную", style: .alert)
                self.switchFirstVC.isOn = false
                self.buttonFirstVC.isUserInteractionEnabled = true
                self.latitudeTextFieldFVC.isUserInteractionEnabled = true
                self.longitudeTextFieldFVC.isUserInteractionEnabled = true
            }
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
        print("Can,t get your location ")
    }
    
}


