//
//  NetworkManager.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 06.09.2020.
//  Copyright © 2020 Михаил Копейкин. All rights reserved.
//

import Foundation

class NetworkManager {
    
    
    
    private init(){}
    
    static let sharet:NetworkManager = NetworkManager()
    
    func getWeather(latitude:String,longitude:String,units:String, result:@escaping((OfferModel?)-> ())) {
        
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "api.openweathermap.org"
        urlComponents.path = "/data/2.5/forecast"
        urlComponents.queryItems = [URLQueryItem(name: "lat", value: latitude),
                                    URLQueryItem(name: "lon", value: longitude),
                                    URLQueryItem(name: "units", value: units),
                                    URLQueryItem(name: "lang", value: "ru"),
                                    URLQueryItem(name: "appid", value: "e2213c9e4a3c9b3b511d6eb55e9abc71")]
        var request = URLRequest(url: urlComponents.url!)
        request.httpMethod = "GET"
        
        let task = URLSession(configuration: .default)
        task.dataTask(with: request) {
            (data,response,error) in
            
            if error == nil {
                let decoder = JSONDecoder()
                var decoderOfferModel:OfferModel?
               
                
                if data != nil {
                    decoderOfferModel = try? decoder.decode(OfferModel.self, from: data!)
                }
                result(decoderOfferModel)
            }else{
                print(error as Any)
            }
        }.resume()
        
    }
}
