//
//  Switch.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 12.01.2021.
//  Copyright © 2021 Михаил Копейкин. All rights reserved.
//

import Foundation
import UIKit

class SwitchFirstVC:UISwitch {
    
    
    required init?(coder aDecoder:NSCoder) {
        super.init(coder:aDecoder)
        initializeSwitch()
    }
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        initializeSwitch()
    }
    
    
    func initializeSwitch(){
//        //щттенок выкл сост
//        self.tintColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        //рычажок
        self.thumbTintColor = UIColor.white
        //on
        self.onTintColor = UIColor.blue
        
    }
}
