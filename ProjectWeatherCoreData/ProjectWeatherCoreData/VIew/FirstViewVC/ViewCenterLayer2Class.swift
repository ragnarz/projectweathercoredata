//
//  ViewCenterLayer2Class.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 04.01.2021.
//  Copyright © 2021 Михаил Копейкин. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class ViewCenterLayer2Class:UIView {
    
    private let shadowViewLayer2Class = ShadowViewLayer2Class()
        
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            initializeViewCentrClass()
        }
        
        override init(frame: CGRect) {
            super.init(frame:frame)
            initializeViewCentrClass()
        }
        
        func initializeViewCentrClass() {
            
            self.addSubview(shadowViewLayer2Class)
            shadowViewLayer2Class.translatesAutoresizingMaskIntoConstraints = false

//            shadowViewLayer2Class.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
//            shadowViewLayer2Class.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
//            shadowViewLayer2Class.topAnchor.constraint(equalTo: topAnchor).isActive = true
//            shadowViewLayer2Class.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
            
            shadowViewLayer2Class.snp.makeConstraints { (make) in
                make.leadingMargin.trailingMargin.topMargin.bottomMargin.equalToSuperview()
            }

        }
    }

