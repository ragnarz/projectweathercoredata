//
//  ShadowViewCenter.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 02.01.2021.
//  Copyright © 2021 Михаил Копейкин. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class ShadowViewCentr: UIView {
    
    var cornerRadius:CGFloat = 16.0 {
        didSet {
            viewCenterClass.layer.cornerRadius = cornerRadius
        }
    }
    
    var shadowColor:UIColor = UIColor(red: 0.698, green: 0.459, blue: 0.808, alpha: 0.5) {
        
        didSet {
            layer.shadowColor = shadowColor.cgColor
        }
    }
    
    var shadowOpacity: Float = 1.0 {
        didSet {
            layer.shadowOpacity = shadowOpacity
        }
    }
    
    var shadowOffset = CGSize(width: 7.0, height: 5.0){
        didSet {
            layer.shadowOffset = shadowOffset
        }
    }
    
    var shadowRadius:CGFloat = 8.0 {
        didSet {
            layer.shadowRadius = shadowRadius
        }
    }
    
    private let viewCenterClass = UIView()
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setupShadow()
    }
    
    func blurView()
       {
           let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
           let blurEffectView = UIVisualEffectView(effect: blurEffect)
           blurEffectView.frame = viewCenterClass.bounds

           blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
           viewCenterClass.addSubview(blurEffectView)
       }
    
    private func setupShadow() {
        self.layer.shadowColor = shadowColor.cgColor
        layer.shadowRadius = shadowRadius
        layer.shadowOpacity = shadowOpacity
        layer.shadowOffset = shadowOffset

        
        let cgPath = UIBezierPath(roundedRect: bounds, byRoundingCorners: [.allCorners], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius)).cgPath
        self.layer.shadowPath = cgPath
    }
    
    private func setup() {
        
        self.addSubview(viewCenterClass)
        
       
        viewCenterClass.translatesAutoresizingMaskIntoConstraints = false

//        viewCenterClass.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
//        viewCenterClass.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
//        viewCenterClass.topAnchor.constraint(equalTo: topAnchor).isActive = true
//        viewCenterClass.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        
        viewCenterClass.layer.borderWidth = 1
        viewCenterClass.layer.borderColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2).cgColor
        viewCenterClass.backgroundColor = .clear
        viewCenterClass.layer.cornerRadius = cornerRadius
        viewCenterClass.clipsToBounds = true
        blurView()
        
        viewCenterClass.snp.makeConstraints { (make) in
            make.leadingMargin.trailingMargin.topMargin.bottomMargin.equalToSuperview()
        }
        
        
    }
}
