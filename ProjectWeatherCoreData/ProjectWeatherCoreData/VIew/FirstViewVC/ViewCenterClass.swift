//
//  ViewCenterClass.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 30.12.2020.
//  Copyright © 2020 Михаил Копейкин. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class ViewCenterClass : UIView {
   
    private let shadowViewCenter = ShadowViewCentr()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeViewCentrClass()
    }
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        initializeViewCentrClass()
    }
    
    func initializeViewCentrClass() {
        
        self.addSubview(shadowViewCenter)
        shadowViewCenter.translatesAutoresizingMaskIntoConstraints = false
        
        shadowViewCenter.snp.makeConstraints { (make) in
            make.leadingMargin.trailingMargin.topMargin.bottomMargin.equalToSuperview()
        }
        
//        shadowViewCenter.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
//        shadowViewCenter.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
//        shadowViewCenter.topAnchor.constraint(equalTo: topAnchor).isActive = true
//        shadowViewCenter.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        
    }
}

extension ViewCenterClass{
    func blurView()
    {
        if #available(iOS 13.0, *) {
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.systemUltraThinMaterial)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.alpha = 0.95

            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
            self.addSubview(blurEffectView)
        } else {
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.alpha = 0.95

            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
            self.addSubview(blurEffectView)
        }
        
    }
}

