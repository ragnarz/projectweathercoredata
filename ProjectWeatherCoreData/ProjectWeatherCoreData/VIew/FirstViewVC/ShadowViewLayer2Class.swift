//
//  ShadowViewLayer2Class.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 04.01.2021.
//  Copyright © 2021 Михаил Копейкин. All rights reserved.
//

import Foundation
import UIKit

class ShadowViewLayer2Class: UIView {
    
    var cornerRadius:CGFloat = 16.0 {
        didSet {
            viewCenterLayer2Class.layer.cornerRadius = cornerRadius
        }
    }
    
    var shadowColor:UIColor = UIColor(red: 0.8940709829, green: 0.7608312368, blue: 0.9018542171, alpha: 0.7) {
        
        didSet {
            layer.shadowColor = shadowColor.cgColor
        }
    }
    
    var shadowOpacity: Float = 1.0 {
        didSet {
            layer.shadowOpacity = shadowOpacity
        }
    }
    
    var shadowOffset = CGSize(width: 7.0, height: 5.0){
        didSet {
            layer.shadowOffset = shadowOffset
        }
    }
    
    var shadowRadius:CGFloat = 4.0 {
        didSet {
            layer.shadowRadius = shadowRadius
        }
    }
    
    private let viewCenterLayer2Class = UIView()
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setupShadow()
    }
    
    func blurView()
       {
        if #available(iOS 13.0, *) {
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.systemUltraThinMaterial)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.alpha = 0.95
            blurEffectView.frame = viewCenterLayer2Class.bounds

            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
            viewCenterLayer2Class.addSubview(blurEffectView)
        } else {
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.alpha = 0.95
            blurEffectView.frame = viewCenterLayer2Class.bounds

            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
            viewCenterLayer2Class.addSubview(blurEffectView)
        }
           
       }
    
    private func setupShadow() {
        self.layer.shadowColor = shadowColor.cgColor
        layer.shadowRadius = shadowRadius
        layer.shadowOpacity = shadowOpacity
        layer.shadowOffset = shadowOffset

        let cgPath = UIBezierPath(roundedRect: bounds, byRoundingCorners: [.allCorners], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius)).cgPath
        self.layer.shadowPath = cgPath
    }
    
    private func setup() {
        
        self.addSubview(viewCenterLayer2Class)
        
        viewCenterLayer2Class.translatesAutoresizingMaskIntoConstraints = false

//        viewCenterLayer2Class.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
//        viewCenterLayer2Class.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
//        viewCenterLayer2Class.topAnchor.constraint(equalTo: topAnchor).isActive = true
//        viewCenterLayer2Class.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        viewCenterLayer2Class.layer.borderWidth = 1
        viewCenterLayer2Class.layer.borderColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2).cgColor
        viewCenterLayer2Class.backgroundColor = .clear
        viewCenterLayer2Class.layer.cornerRadius = cornerRadius
        viewCenterLayer2Class.clipsToBounds = true
        blurView()
        
        viewCenterLayer2Class.snp.makeConstraints { (make) in
            make.leadingMargin.trailingMargin.topMargin.bottomMargin.equalToSuperview()
        }
 
    }
}
