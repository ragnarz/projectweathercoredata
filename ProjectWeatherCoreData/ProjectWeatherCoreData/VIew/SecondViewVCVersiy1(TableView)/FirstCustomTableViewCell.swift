//
//  FirstCustomTableViewCell.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 04.11.2020.
//  Copyright © 2020 Михаил Копейкин. All rights reserved.
//

import Foundation
import UIKit

class FirstCustomTableViewCell:UITableViewCell {
    
    @IBOutlet weak var firstCityLable: UILabel!
    @IBOutlet weak var firstIconImag: UIImageView!
    @IBOutlet weak var firstTempLable: UILabel!
    
    
    override var reuseIdentifier: String?{
        return "FirstCustomTableViewCell"
    }
    
    //задаём нужный тип данных для отображения imag
    private func getDataForImag(_ imagStringModel:String) -> Data {
        let inputString = imagStringModel
        
        
        let iconURL = URL(string: "http://openweathermap.org/img/wn/\(inputString)@2x.png")
        let dataIcon = try? Data(contentsOf: iconURL!)
        
        return dataIcon!
    }
    
    
    func setup(attr:FirstCustomTableViewCellAttribut){
        self.firstTempLable.text = "\((attr.firstTemp))°C"
        self.firstCityLable.text = attr.firstCityName
        self.firstIconImag.image = UIImage(data: getDataForImag(attr.firstIcon))
        
    }
    
}

struct FirstCustomTableViewCellAttribut {
    let firstTemp:String
    let firstCityName:String
    let firstIcon:String
    
}
