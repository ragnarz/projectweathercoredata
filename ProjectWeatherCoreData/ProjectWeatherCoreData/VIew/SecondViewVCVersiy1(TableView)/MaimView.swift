//
//  MaimView.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 01.10.2020.
//  Copyright © 2020 Михаил Копейкин. All rights reserved.
//

import Foundation
import UIKit

class MainVIew: UIView {
    
    var tableView = UITableView()
    
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        setupConstrains()
        self.firtInitialization()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder:coder)
        setupConstrains()
        self.firtInitialization()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.firtInitialization()
        self.setupConstrains()
        
        
    }
    
    //регистрация ячейки
    fileprivate func firtInitialization(){
        self.tableView.register(ListOfForecastDatesTableViewCell.self, forCellReuseIdentifier: "ListOfForecastDatesTableViewCell1")
        self.tableView.backgroundColor = #colorLiteral(red: 0.2350804508, green: 0.6000443101, blue: 0.9057689309, alpha: 1)
//        self.tableView.register(UINib(nibName: "ListOfForecastDatesTableViewCell", bundle: nil), forCellReuseIdentifier: "ListOfForecastDatesTableViewCell1")
        self.addSubview(tableView)
    }
    
    
    
    fileprivate func setupConstrains(){
        
        self.addSubview(tableView)
        
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        
        self.tableView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        self.tableView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        self.tableView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        self.tableView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
         self.tableView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
          self.tableView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }
}


