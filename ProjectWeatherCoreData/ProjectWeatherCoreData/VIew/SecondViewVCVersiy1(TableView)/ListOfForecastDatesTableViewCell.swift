//
//  ListOfForecastDatesTableViewCell.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 07.09.2020.
//  Copyright © 2020 Михаил Копейкин. All rights reserved.
//

import Foundation
import UIKit



class ListOfForecastDatesTableViewCell:UITableViewCell {
    
    var tempLable : LableFirstVC = {
        var tempLab = LableFirstVC()
        tempLab.frame = CGRect(x: 290, y: 65, width: 95, height: 50)
//        tempLab.textColor = .w
        tempLab.backgroundColor = .clear
        
        return tempLab
    }()
    
    var cityLable:LableFirstVC = {
        var cityLab = LableFirstVC()
        cityLab.frame = CGRect(x: 10, y: 70, width: 150, height: 30)
//        cityLab.textColor = .black
        cityLab.backgroundColor = .clear
        return cityLab
    }()
    
    var iconImag:UIImageView = {
        var iconIm = UIImageView()
        iconIm.frame = CGRect(x: 160, y: 40, width: 80, height: 80)
        
        return iconIm
    }()
    
    var timeLable:LableFirstVC = {
        var timeLab = LableFirstVC()
        timeLab.frame = CGRect(x: 80, y: 140, width: 250, height: 30)
//        timeLab.textColor = .black
        timeLab.backgroundColor = .clear
        return timeLab
    }()
    
    
    override var reuseIdentifier: String?{
        return "ListOfForecastDatesTableViewCell1"
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style:style,reuseIdentifier:reuseIdentifier)
        self.addSubview(tempLable)
        self.addSubview(cityLable)
        self.addSubview(iconImag)
        self.addSubview(timeLable)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder:coder)
    }
    
    //задаём нужный формат дате
    private    func getDayForDate(_ date:String?) -> String {
            guard let inputDate = date else {return ""}
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "EEEE, MMM d, yyyy"
        dateFormatterPrint.locale = Locale(identifier: "ru_RU")
            
            //        if let date = dateFormatterGet.date(from: inputDate) {
            //            print(dateFormatterPrint.string(from: date))
            //        } else {
            //           print("There was an error decoding the string")
            //        }
            let date = dateFormatterGet.date(from: inputDate)
            let dateStr = dateFormatterPrint.string(from: date!)
            
            
            return dateStr
        }
    
    
    private  func getTimForDate(_ date:String?) -> String {
        guard let inputDate = date else {return ""}
        
        let dateFormatterGet = DateFormatter()
        
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        
        let dateFormatterPrint = DateFormatter()
        
        dateFormatterPrint.dateFormat = "HH:mm:ss"
        
        
        
        //        if let date = dateFormatterGet.date(from: inputDate) {
        //            print(dateFormatterPrint.string(from: date))
        //        } else {
        //           print("There was an error decoding the string")
        //        }
        let date = dateFormatterGet.date(from: inputDate)
        
        let dateStr = dateFormatterPrint.string(from: date!)
        
        
        return dateStr
    }
    
    //задаём нужный тип данных для отображения imag
    
    private func getDataForImag(_ imagStringModel:String) -> Data {
         let inputString = imagStringModel
        
         
        let iconURL = URL(string: "http://openweathermap.org/img/wn/\(inputString)@2x.png")
        let dataIcon = try? Data(contentsOf: iconURL!)
        
        return dataIcon!
    }
    
    
    func setup(attr:ListOfForecastDatesTableViewCellAttribut){
        self.tempLable.text = "\((attr.temp))°C"
        self.cityLable.text = getTimForDate(attr.time)
        self.iconImag.image = UIImage(data: getDataForImag(attr.icon))
        self.timeLable.text = getDayForDate(attr.time)
    }

}

struct ListOfForecastDatesTableViewCellAttribut {
    let temp:String
    let cityName:String
    let icon:String
    let time:String
}
