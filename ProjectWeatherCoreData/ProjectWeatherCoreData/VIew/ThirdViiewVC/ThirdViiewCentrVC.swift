//
//  ThirdViiewCentrVC.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 05.01.2021.
//  Copyright © 2021 Михаил Копейкин. All rights reserved.
//

import Foundation
import UIKit

class ThirdViiewCentrVC: UIView {
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeThirdViewCentrClass()
    }
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        initializeThirdViewCentrClass()
    }
    
    func initializeThirdViewCentrClass() {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2).cgColor
        self.backgroundColor = .clear
        self.layer.cornerRadius = 16
        self.clipsToBounds = true
        self.blurView()
}
}
    
extension ThirdViiewCentrVC {
    func blurView()
    {
        if #available(iOS 13.0, *) {
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.systemUltraThinMaterial)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.alpha = 0.95
            blurEffectView.frame = self.bounds

            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
            self.addSubview(blurEffectView)
        } else {
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.alpha = 0.95
            blurEffectView.frame = self.bounds

            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
            self.addSubview(blurEffectView)
        }
        
    }
}
