//
//  ThirdViiewCentrVCLayer2.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 06.01.2021.
//  Copyright © 2021 Михаил Копейкин. All rights reserved.
//

import Foundation
import UIKit

class ThirdViiewCentrVCLayer2: UIView {
    
    required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            initializeThirdViewCentrLayer2()
        }
        
        override init(frame: CGRect) {
            super.init(frame:frame)
            initializeThirdViewCentrLayer2()
        }
        
        func initializeThirdViewCentrLayer2() {
            
            self.backgroundColor = .clear
    }
}
