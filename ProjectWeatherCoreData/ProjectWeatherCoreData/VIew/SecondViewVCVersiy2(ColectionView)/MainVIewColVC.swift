//
//  MainVIewColVC.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 07.01.2021.
//  Copyright © 2021 Михаил Копейкин. All rights reserved.
//

import Foundation
import UIKit

class MainVIewColVC: UIView {
    
    var secondViewCentrVC = SecondViewCenterClass()
    let firstViewCenterClass = FirstViewCenterClass()

    override init(frame: CGRect) {
        super.init(frame:frame)
        self.firtInitialization()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder:coder)
        self.firtInitialization()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.firtInitialization()
        
    }

    //регистрация ячейки
    fileprivate func firtInitialization(){
        setupConstrains()
//        self.coltctionView.register(UINib(nibName: "ListOfForecastDatesCollectionViewCell", bundle: nil), forCellReuseIdentifier: "ListOfForecastDatesCollectionViewCell")
        
    }

    fileprivate func setupConstrains(){
        
        self.backgroundColor = #colorLiteral(red: 0.9137169719, green: 0.5490475297, blue: 0.317629993, alpha: 1)
        
        self.addSubview(secondViewCentrVC)
        secondViewCentrVC.translatesAutoresizingMaskIntoConstraints = false
        self.secondViewCentrVC.snp.makeConstraints { (make) in
//            make.size.equalTo(CGSize(width: 280.34, height: 330.0))
            make.center.equalToSuperview()
            make.edges.equalTo(UIEdgeInsets(top: 159.25, left: 18.0, bottom: 50.6, right: 17.83))

        }
        
//        secondViewCentrVC.translatesAutoresizingMaskIntoConstraints = false
//        self.secondViewCentrVC.widthAnchor.constraint(equalToConstant: 339.17).isActive = true
//        self.secondViewCentrVC.heightAnchor.constraint(equalToConstant: 360.15).isActive = true
//        self.secondViewCentrVC.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 15.83).isActive = true
//        self.secondViewCentrVC.topAnchor.constraint(equalTo: self.topAnchor, constant: 159.25).isActive = true
        
        self.addSubview(firstViewCenterClass)
        firstViewCenterClass.translatesAutoresizingMaskIntoConstraints = false
        self.firstViewCenterClass.snp.makeConstraints { (make) in
//            make.size.equalTo(CGSize(width: 262.25, height: 308.71))
            make.center.equalToSuperview()
            make.edges.equalTo(UIEdgeInsets(top: 116.34, left: 18.0, bottom: 50.6, right: 17.83))

        }
        
        
        //        self.firstViewCenterClass.translatesAutoresizingMaskIntoConstraints = false
//        self.firstViewCenterClass.widthAnchor.constraint(equalToConstant: 339.17).isActive = true
//        self.firstViewCenterClass.heightAnchor.constraint(equalToConstant: 403.06).isActive = true
//        self.firstViewCenterClass.leadingAnchor.constraint(equalTo: secondViewCentrVC.leadingAnchor).isActive = true
//        self.firstViewCenterClass.bottomAnchor.constraint(equalTo: secondViewCentrVC.bottomAnchor).isActive = true
        
        
        
        
       
    }
}
