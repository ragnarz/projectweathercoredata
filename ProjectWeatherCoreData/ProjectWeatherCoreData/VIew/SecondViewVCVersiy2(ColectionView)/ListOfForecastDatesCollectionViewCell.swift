//
//  ListOfForecastDatesCollectionViewCell.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 07.01.2021.
//  Copyright © 2021 Михаил Копейкин. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class ListOfForecastDatesCollectionViewCell: UICollectionViewCell {
    
    var cornerRadius:CGFloat = 16.0 {
        didSet {
            secondViewCell.layer.cornerRadius = cornerRadius
        }
    }
    
    var shadowColor:UIColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25) {
        
        didSet {
            layer.shadowColor = shadowColor.cgColor
        }
    }
    
    var shadowOpacity: Float = 1.0 {
        didSet {
            layer.shadowOpacity = shadowOpacity
        }
    }
    
    var shadowOffset = CGSize(width: 0.0, height: 4.0){
        didSet {
            layer.shadowOffset = shadowOffset
        }
    }
    
    var shadowRadius:CGFloat = 4.0 {
        didSet {
            layer.shadowRadius = shadowRadius
        }
    }
    
     let secondViewCell = SecondViewCell()
    
    
    
    override var reuseIdentifier: String?{
        return "ListOfForecastDatesCollectionViewCell"
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupCell()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder:coder)
        setupCell()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setupShadowCell()
    }
    
    private func setupShadowCell() {
        self.layer.shadowColor = shadowColor.cgColor
        layer.shadowRadius = shadowRadius
        layer.shadowOpacity = shadowOpacity
        layer.shadowOffset = shadowOffset

        
        let cgPath = UIBezierPath(roundedRect: bounds, byRoundingCorners: [.allCorners], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius)).cgPath
        self.layer.shadowPath = cgPath
    }
    
    private func setupCell() {
        
        self.addSubview(secondViewCell)
        
       
        secondViewCell.translatesAutoresizingMaskIntoConstraints = false

//        secondViewCell.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
//        secondViewCell.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
//        secondViewCell.topAnchor.constraint(equalTo: topAnchor).isActive = true
//        secondViewCell.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true

        secondViewCell.snp.makeConstraints { (make) in
            make.leadingMargin.trailingMargin.topMargin.bottomMargin.equalToSuperview()
        }
    }
    
    
    
}
