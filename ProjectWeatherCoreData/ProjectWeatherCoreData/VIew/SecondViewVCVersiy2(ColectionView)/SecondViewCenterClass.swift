//
//  SecondViewCenterClass.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 10.01.2021.
//  Copyright © 2021 Михаил Копейкин. All rights reserved.
//

import Foundation
import UIKit

class SecondViewCenterClass: UIView {
    
//    let firstViewCenterClass = FirstViewCenterClass()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeViewCentrClass()
    }
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        initializeViewCentrClass()
    }
    
    func initializeViewCentrClass() {
        
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.2).cgColor
        self.backgroundColor = .clear
        self.layer.cornerRadius = 16.0
        self.clipsToBounds = true
        blurView()
        
//        self.addSubview(firstViewCenterClass)
//        self.backgroundColor = .red
//        self.firstViewCenterClass.translatesAutoresizingMaskIntoConstraints = false
//        self.firstViewCenterClass.widthAnchor.constraint(equalToConstant: 339.17).isActive = true
//        self.firstViewCenterClass.heightAnchor.constraint(equalToConstant: 403.06).isActive = true
//        self.firstViewCenterClass.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
//        self.firstViewCenterClass.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
//        
    }
}

extension SecondViewCenterClass{
    func blurView()
    {
        if #available(iOS 13.0, *) {
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.systemUltraThinMaterial)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.alpha = 0.95
            blurEffectView.frame = self.bounds
            
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
            self.addSubview(blurEffectView)
        } else {
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.alpha = 0.95
            blurEffectView.frame = self.bounds
            
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
            self.addSubview(blurEffectView)
        }
        
    }
}


