//
//  FirstViewCenterClass.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 11.01.2021.
//  Copyright © 2021 Михаил Копейкин. All rights reserved.
//

import Foundation
import UIKit

class FirstViewCenterClass: UIView {
    
    var coltctionView:UICollectionView =  {
    let layout = UICollectionViewFlowLayout()
    layout.scrollDirection = .horizontal
    let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
    cv.register(ListOfForecastDatesCollectionViewCell.self, forCellWithReuseIdentifier: "ListOfForecastDatesCollectionViewCell")
        cv.showsHorizontalScrollIndicator = false
    cv.backgroundColor = .clear
    return cv
    }()
    
    var tempLable : LableFirstVC = {
        var tempLab = LableFirstVC()
        tempLab.frame = CGRect(x: 30.07, y: 150.91, width: 130, height: 22)
        tempLab.textColor = .white
        tempLab.backgroundColor = .clear
        tempLab.font = UIFont(name: "SFProDisplay-Black", size: 30)
        
        var paragraphStyle = NSMutableParagraphStyle()
        
        paragraphStyle.lineHeightMultiple = 0.61
        
        // Line height: 22 pt
        
        // (identical to box height)
        
        tempLab.attributedText = NSMutableAttributedString(string: "", attributes: [NSAttributedString.Key.kern: -0.41, NSAttributedString.Key.paragraphStyle: paragraphStyle])
        tempLab.textAlignment = .left
        return tempLab
        }()
        
        var cityLable:LableFirstVC = {
            var cityLab = LableFirstVC()
            cityLab.frame = CGRect(x: 30.07, y: 90.91, width: 200, height: 46)
            cityLab.textColor = .white
            cityLab.font = UIFont(name: "SFProDisplay-Heavy", size: 20)

            var paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineHeightMultiple = 0.92

            // Line height: 22 pt

            // (identical to box height)

            cityLab.attributedText = NSMutableAttributedString(string: "", attributes: [NSAttributedString.Key.kern: -0.41, NSAttributedString.Key.paragraphStyle: paragraphStyle])
            cityLab.backgroundColor = .clear
            cityLab.textAlignment = .left
            return cityLab
        }()
        
        var iconImag:UIImageView = {
            var iconIm = UIImageView()
            iconIm.frame = CGRect(x: 16.99, y: 0.0, width: 117.00, height: 73.00)
            iconIm.contentMode = .scaleAspectFill
            
            return iconIm
        }()
        
        var timeLable:LableFirstVC = {
            var timeLab = LableFirstVC()
            timeLab.frame = CGRect(x: 30.07, y: 210.91, width: 120, height: 22)
            timeLab.textColor = .white 
            timeLab.backgroundColor = .clear
            timeLab.font = UIFont(name: "SFProDisplay-Regular", size: 14)
            var paragraphStyle = NSMutableParagraphStyle()

            paragraphStyle.lineHeightMultiple = 1.32


            // Line height: 22 pt

            // (identical to box height)


            timeLab.textAlignment = .left

            timeLab.attributedText = NSMutableAttributedString(string: "", attributes: [NSAttributedString.Key.kern: -0.41, NSAttributedString.Key.paragraphStyle: paragraphStyle])
            
            return timeLab
        }()
    
    required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            initializeViewCentrClass()
        }
        
        override init(frame: CGRect) {
            super.init(frame:frame)
            initializeViewCentrClass()
        }
    
        private func getDataForImag(_ imagStringModel:String) -> Data {
                let inputString = imagStringModel
               
                
               let iconURL = URL(string: "http://openweathermap.org/img/wn/\(inputString)@2x.png")
               let dataIcon = try? Data(contentsOf: iconURL!)
               
               return dataIcon!
           }
    
    private    func getDayForDate(_ date:String?) -> String {
        guard let inputDate = date else {return ""}
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "d MMMM yyyy"
    dateFormatterPrint.locale = Locale(identifier: "ru_RU")
        
        //        if let date = dateFormatterGet.date(from: inputDate) {
        //            print(dateFormatterPrint.string(from: date))
        //        } else {
        //           print("There was an error decoding the string")
        //        }
        let date = dateFormatterGet.date(from: inputDate)
        let dateStr = dateFormatterPrint.string(from: date!)
        
        
        return dateStr
    }
    
        func initializeViewCentrClass() {

            self.addSubview(coltctionView)
            
            self.coltctionView.snp.makeConstraints { (make) in
                make.size.equalTo(CGSize(width: 277.14, height: 128.52))
//                make.edges.equalTo(UIEdgeInsets(top: 239.38, left: 30.0, bottom: 39.0, right: 30.02))
                make.centerX.equalToSuperview()
//                make.centerY.equalToSuperview()
                make.left.equalToSuperview().offset(30.0)
                make.right.equalToSuperview().offset(-30.0)
//                make.top.equalToSuperview().offset(239.38)
                make.bottom.equalToSuperview().offset(-39.0)
                
                
            }
            
            self.addSubview(iconImag)
            self.addSubview(cityLable)
            self.addSubview(tempLable)
            self.addSubview(timeLable)
            
            self.backgroundColor = .clear
            //            self.coltctionView.translatesAutoresizingMaskIntoConstraints = false
            //            self.coltctionView.widthAnchor.constraint(equalToConstant: 287.14).isActive = true
            //            self.coltctionView.heightAnchor.constraint(equalToConstant: 128.52).isActive = true
            //            self.coltctionView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 23.00).isActive = true
            //            self.coltctionView.topAnchor.constraint(equalTo: self.topAnchor, constant: 239.38).isActive = true
    }
    
    func setup(attr:FirstViewCenterClassAttribut){
        self.tempLable.text = "\((attr.firstTemp))°C"
        self.cityLable.text = attr.firstCityName
        self.iconImag.image = UIImage(data: getDataForImag(attr.firstIcon))
        self.timeLable.text = getDayForDate(attr.firstTime)
            
        }
}
    

    struct FirstViewCenterClassAttribut {
        let firstTemp:String
        let firstCityName:String
        let firstIcon:String
        let firstTime:String
        
    
    
    }
    

