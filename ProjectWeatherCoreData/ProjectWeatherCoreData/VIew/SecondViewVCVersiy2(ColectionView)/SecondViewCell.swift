//
//  SecondViewCell.swift
//  ProjectWeatherCoreData
//
//  Created by Михаил Копейкин on 14.01.2021.
//  Copyright © 2021 Михаил Копейкин. All rights reserved.
//

import Foundation
import UIKit

class SecondViewCell: UIView {
    
    var tempLable : LableFirstVC = {
        var tempLab = LableFirstVC()
        tempLab.frame = CGRect(x: 1.07, y: 40.76, width: 60.0, height: 22.0)
        tempLab.backgroundColor = .clear
        tempLab.font = UIFont(name: "SFProDisplay-Bold", size: 14)

        var paragraphStyle = NSMutableParagraphStyle()

        paragraphStyle.lineHeightMultiple = 1.32


        // Line height: 22 pt

        // (identical to box height)

        tempLab.attributedText = NSMutableAttributedString(string: "", attributes: [NSAttributedString.Key.kern: -0.41, NSAttributedString.Key.paragraphStyle: paragraphStyle])
        
        return tempLab
    }()
    
    var cityLable:LableFirstVC = {
        var cityLab = LableFirstVC()
        cityLab.frame = CGRect(x: 0, y: 70, width: 150, height: 30)
        //        cityLab.textColor = .black
        cityLab.backgroundColor = .clear
        return cityLab
    }()
    
    var iconImag:UIImageView = {
        var iconIm = UIImageView()
        iconIm.frame = CGRect(x: -4, y: 50.81, width: 70.02, height: 70.17)
        iconIm.contentMode = .scaleAspectFill
        
        return iconIm
    }()
    
    var timeLable:LableFirstVC = {
        var timeLab = LableFirstVC()
        timeLab.frame = CGRect(x: 9.92, y: 14.0, width: 40, height: 22)
        timeLab.backgroundColor = .clear

        timeLab.font = UIFont(name: "SFProDisplay-Regular", size: 14)

        var paragraphStyle = NSMutableParagraphStyle()

        paragraphStyle.lineHeightMultiple = 1.32

        // Line height: 22 pt

        // (identical to box height)

        timeLab.attributedText = NSMutableAttributedString(string: "", attributes: [NSAttributedString.Key.kern: -0.41, NSAttributedString.Key.paragraphStyle: paragraphStyle])
        
        return timeLab
    }()
 
    override init(frame: CGRect) {
            super.init(frame: frame)
            
        self.initializeViewCentrClass()
   
        }
        
        required init?(coder: NSCoder) {
            super.init(coder:coder)
            self.initializeViewCentrClass()
        }
    
    func initializeViewCentrClass() {
        
                self.blurView()
        
        self.layer.cornerRadius = 16
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5).cgColor
        self.clipsToBounds = true
        
        self.addSubview(tempLable)
        //        self.addSubview(cityLable)
        self.addSubview(iconImag)
        self.addSubview(timeLable)

        
        
    }
    
    //задаём нужный формат дате
    private    func getDayForDate(_ date:String?) -> String {
        guard let inputDate = date else {return ""}
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "HH:mm"
        dateFormatterPrint.locale = Locale(identifier: "ru_RU")
        
        //        if let date = dateFormatterGet.date(from: inputDate) {
        //            print(dateFormatterPrint.string(from: date))
        //        } else {
        //           print("There was an error decoding the string")
        //        }
        let date = dateFormatterGet.date(from: inputDate)
        let dateStr = dateFormatterPrint.string(from: date!)
        
        
        return dateStr
    }
    
    
    private  func getTimForDate(_ date:String?) -> String {
        guard let inputDate = date else {return ""}
        
        let dateFormatterGet = DateFormatter()
        
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        
        let dateFormatterPrint = DateFormatter()
        
        dateFormatterPrint.dateFormat = "HH:mm:ss"
        
        
        
        //        if let date = dateFormatterGet.date(from: inputDate) {
        //            print(dateFormatterPrint.string(from: date))
        //        } else {
        //           print("There was an error decoding the string")
        //        }
        let date = dateFormatterGet.date(from: inputDate)
        
        let dateStr = dateFormatterPrint.string(from: date!)
        
        
        return dateStr
    }
    
    //задаём нужный тип данных для отображения imag
    
    private func getDataForImag(_ imagStringModel:String) -> Data {
        let inputString = imagStringModel
        
        
        let iconURL = URL(string: "http://openweathermap.org/img/wn/\(inputString)@2x.png")
        let dataIcon = try? Data(contentsOf: iconURL!)
        
        return dataIcon!
    }
    
    
    func setup(attr:ListOfForecastDatesCollectionViewCellAttribut){
        self.tempLable.text = "\((attr.temp))°C"
        self.cityLable.text = getTimForDate(attr.time)
        self.iconImag.image = UIImage(data: getDataForImag(attr.icon))
        self.timeLable.text = getDayForDate(attr.time)
    }
    
}


extension SecondViewCell{
    func blurView()
    {
        if #available(iOS 13.0, *) {
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.systemMaterial)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.alpha = 0.7

            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
            self.addSubview(blurEffectView)
        } else {
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.extraLight)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.alpha = 0.7

            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
            self.addSubview(blurEffectView)
        }
        
    }
}

struct ListOfForecastDatesCollectionViewCellAttribut {
    let temp:String
    let cityName:String
    let icon:String
    let time:String
}
